#!/usr/bin/env python
"""Script to create an xml file for shape predictor."""
import os

# from skimage import io

# import xml.etree.cElementTree as ET
from xml.dom import minidom

import sys

import cv2

import dlib


TYPE = sys.argv[1]
SET = sys.argv[2]
DETECTOR = sys.argv[3] + '.svm'
COORDX = None
COORDY = None


def clean(data):
    """Clean the data from the camera."""
    new = str(data)
    t = new.split(" ")
    up = int(float((t[0][2:-1])))
    low = int(float((t[1][:-1])))
    left = int(float((t[2][1:-1])))
    right = int(float((t[3][:-2])))
    return [up, low, left, right]


def click_and_select(event, posx, posy, flags, param):
    """Method to manually select the feature points."""
    global COORDX, COORDY
    if event == cv2.EVENT_LBUTTONDBLCLK:
        COORDX, COORDY = int(posx), int(posy)
        # print COORDX, COORDY

CURRENT_PATH = os.path.dirname(os.path.realpath(__file__))
# DETECTOR_PATH = CURRENT_PATH + '/../detectors/detector_keith.svm'
DETECTOR_PATH = os.path.join(CURRENT_PATH, DETECTOR)

TRAINING_PATH = os.path.join(CURRENT_PATH, 'images', TYPE, SET)
TRAINING_IMAGES = os.listdir(TRAINING_PATH)
print len(TRAINING_IMAGES)
# os.path.join

DETECTOR = dlib.simple_object_detector(DETECTOR_PATH)

doc = minidom.Document()
# DATASET = doc.createElement('dataset')
# doc.appendChild(DATASET)
IMAGES = doc.createElement('images')
doc.appendChild(IMAGES)

for i in range(len(TRAINING_IMAGES)):
    print "Imaginea ", i + 1
    img = TRAINING_IMAGES[i]

    img_path = TRAINING_PATH + '/' + img
    imag = cv2.imread(img_path)
    print img
    files = TYPE + '/' + SET + '/' + img
    IMAGE = doc.createElement("image")
    IMAGE.setAttribute('file', files)
    IMAGES.appendChild(IMAGE)

    dets = DETECTOR(imag, 1)

    position = clean(dets[0])
    top = position[1]
    left = position[0]
    width = position[2] - position[0]
    height = position[3] - position[1]

    BOX = doc.createElement("box")
    BOX.setAttribute("top", str(top))
    BOX.setAttribute("left", str(left))
    BOX.setAttribute("width", str(width))
    BOX.setAttribute("height", str(height))
    IMAGE.appendChild(BOX)

    cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('image', 1600, 1600)
    cv2.setMouseCallback('image', click_and_select)

    for i in range(11):
        if i <= 9:
            number = '0' + str(i)
        else:
            number = str(i)
        cv2.imshow('image', imag)
        cv2.rectangle(imag, (position[0], position[1]),
                      (position[2], position[3]), (255, 255, 0), 1)
        key = cv2.waitKey(0) & 0xFF
        if key == ord('s'):
            cv2.circle(imag, (COORDX, COORDY),
                       1, (255, 0, 0), 2)
            PART = doc.createElement("part")
            PART.setAttribute("name", number)
            PART.setAttribute("x", str(COORDX))
            PART.setAttribute("y", str(COORDY))
            BOX.appendChild(PART)
        if key == ord('n'):
            break

    # dlib.hit_enter_to_continue()
FILE_NAME = './xml_files/predictor' + TYPE + SET + '.xml'
XML_STR = doc.toprettyxml(indent=" ")
with open(FILE_NAME, "w") as f:
    f.write(XML_STR)
