#!/usr/bin/env python
"""Script to create an xml file for object detector."""
import os
from xml.dom import minidom
import sys
import cv2
import dlib


TYPE = sys.argv[1]
SET = sys.argv[2]
COORDX = None
COORDY = None
BOXES = []


def clean(data):
    """Clean the data from the camera."""
    new = str(data)
    t = new.split(" ")
    up = int(float((t[0][2:-1])))
    low = int(float((t[1][:-1])))
    left = int(float((t[2][1:-1])))
    right = int(float((t[3][:-2])))
    return [up, low, left, right]


def click_and_select(event, posx, posy, flags, param):
    """Method to manually select the feature points."""
    global COORDX, COORDY, BOXES
    if event == cv2.EVENT_LBUTTONDOWN:
        COORDX, COORDY = int(posx), int(posy)
        BOXES.append([int(posx), int(posy)])
        # print COORDX, COORDY

    elif event == cv2.EVENT_LBUTTONUP:
        BOXES.append([int(posx), int(posy)])

CURRENT_PATH = os.path.dirname(os.path.realpath(__file__))

TRAINING_PATH = os.path.join(CURRENT_PATH, 'images', TYPE, SET)
TRAINING_IMAGES = os.listdir(TRAINING_PATH)
print len(TRAINING_IMAGES)

doc = minidom.Document()
IMAGES = doc.createElement('images')
doc.appendChild(IMAGES)



for i in range(len(TRAINING_IMAGES)):
    
    cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('image', 1600, 1600)
    cv2.setMouseCallback('image', click_and_select)
    
    print "Imaginea ", i + 1
    img = TRAINING_IMAGES[i]

    img_path = TRAINING_PATH + '/' + img
    imag = cv2.imread(img_path)
    
    print img
    files = TYPE + '/' + SET + '/' + img
    IMAGE = doc.createElement("image")
    IMAGE.setAttribute('file', files)
    IMAGES.appendChild(IMAGE)

    
    
    for i in range(2):
        cv2.imshow('image', imag)
        key = cv2.waitKey(0) & 0xFF
        if len(BOXES) > 0:
            position = BOXES
    
            top = position[0][1]
            left = position[0][0]
            width = position[1][0] - position[0][0]
            height = position[1][1] - position[0][1]
            if key == ord('s'):
                cv2.rectangle(imag, (position[0][0], position[0][1]),
                          (position[1][0], position[1][1]), (255, 255, 0), 1)
            elif key == ord('n'):
                break

    BOX = doc.createElement("box")
    BOX.setAttribute("top", str(top))
    BOX.setAttribute("left", str(left))
    BOX.setAttribute("width", str(width))
    BOX.setAttribute("height", str(height))
    IMAGE.appendChild(BOX)
        
    BOXES = []

FILE_NAME = './xml_files/object_' + TYPE + SET + '.xml'
XML_STR = doc.toprettyxml(indent=" ")
with open(FILE_NAME, "w") as f:
    f.write(XML_STR)
