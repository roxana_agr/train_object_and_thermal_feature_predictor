#!/usr/bin/env python
"""Script to generate the final training file."""

import os
import sys
from xml.dom import minidom
import xml.etree.cElementTree as ET
import argparse

DOC = minidom.Document()
DATASET = DOC.createElement('dataset')
DOC.appendChild(DATASET)

NAME = DOC.createElement('name')
NAME.setAttribute('author', 'Roxana Agrigoroaie')
DATASET.appendChild(NAME)

COMMENT = DOC.createElement('comment')
COMMENT.setAttribute('description', 'This is an .xml file needed for the object detector')
DATASET.appendChild(COMMENT)

IMAGES = DOC.createElement('images')
DATASET.appendChild(IMAGES)

PARSER = argparse.ArgumentParser()
PARSER.add_argument('start', help="The number of the xml file from which to start, without .xml")
PARSER.add_argument('stop', help="The number of the xml file at which to stop, without .xml")
PARSER.add_argument('filename', help="The name of the generate xml file")
args = PARSER.parse_args()

# the default location of all xml_files
XML_FOLDER = './xml_files'
XML_START = args.start
XML_STOP = args.stop
FILE_NAME = args.filename

for i in range(int(XML_START), int(XML_STOP)+1):
	filename = 'object_trainset' + str(i) + '.xml'
	new_file = os.path.join(XML_FOLDER, filename)
	
	element = ET.parse(new_file).getroot()
	for xml_el in element.iter('image'):
		image = DOC.createElement('image')
		image.setAttribute('file', '../images/' + xml_el.attrib['file'])
		box = DOC.createElement('box')
		for file_el in xml_el.iter('box'):
			box.setAttribute('height', file_el.attrib['height'])
			box.setAttribute('left', file_el.attrib['left'])
			box.setAttribute('top', file_el.attrib['top'])
			box.setAttribute('width', file_el.attrib['width'])
		image.appendChild(box)
		IMAGES.appendChild(image)

# the default location of the generated xml file
FILE_NAME = './xml_concatenated/object_' + FILE_NAME + '.xml'
XML_STR = DOC.toprettyxml(indent=" ")
with open(FILE_NAME, "w") as f:
    f.write(XML_STR)


